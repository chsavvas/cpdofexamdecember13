package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {
    @Test
    public void testMax() throws Exception {

        int k= new MinMax().findMax(2,4);
        assertEquals("Max", 4, k);

    }
    @Test
    public void testMin() throws Exception {

        int z= new MinMax().findMax(3,1);
        assertEquals("Max", 3, z);

    }

}
